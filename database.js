const {Client} = require("pg");
const dotenv = require('dotenv');
dotenv.config();
const client = new Client(
	process.env.DATABASE_URL
);

client.connect();

exports.execute = function(query, values){
	return new Promise(function(resolve, reject){
		client.query(query, values, function (err, res) {
			if(err){
				reject(err);
			}else{
				resolve(res.rows);
			}
		});
	});
};